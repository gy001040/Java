package UDP;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Scanner;

public class UDPClient {
	private InetAddress ipAddress;
	private int sendPort;
	private int recvPort;
	
	UDPClient(InetAddress serverAddress, int sendPort, int recvPort) throws Exception {
		ipAddress = serverAddress;
		this.sendPort = sendPort;
		this.recvPort = recvPort;
	}
	
	private void start() throws IOException {
		DatagramSocket udpSocket = new DatagramSocket();
		
		System.out.println("Enter message: ");
		Scanner input = new Scanner(System.in);
		String msg = input.nextLine();
		byte buf[] = null;
		
		buf = msg.getBytes();
		DatagramPacket DpSend = new DatagramPacket(buf, buf.length, ipAddress, sendPort);
		
		udpSocket.send(DpSend);
		
		udpSocket.close();
		listenToServer();
	}
	
	private void listenToServer() throws IOException {
		DatagramSocket udpSocket = new DatagramSocket(recvPort);
		byte[] buf = new byte[256];
		DatagramPacket DpReceive = new DatagramPacket(buf, buf.length);
		udpSocket.receive(DpReceive);
		
		String msg = new String(DpReceive.getData()).trim();
		System.out.println("Message from Server: " + msg);
		
		udpSocket.close();
	}
	
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("-- Running UDP Client at " + InetAddress.getLocalHost() + " --");
		InetAddress ipAddress = InetAddress.getLocalHost(); // local IP address
		int sendPort = 7077;
		int recvPort = 7076;
		UDPClient client = new UDPClient(ipAddress, sendPort, recvPort);
		client.start();
	}

}
