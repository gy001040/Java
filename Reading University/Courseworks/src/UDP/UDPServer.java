package UDP;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

public class UDPServer {
	private int sendPort;
	private int recvPort;
	
	public UDPServer(int sendPort, int recvPort) throws SocketException, IOException {
		this.sendPort = sendPort;
		this.recvPort = recvPort;
	}
	
	private void start() throws Exception {
		DatagramSocket ds = new DatagramSocket(recvPort);
		byte[] buf = new byte[256];
		DatagramPacket DpReceive = null;
		
		DpReceive = new DatagramPacket(buf, buf.length);
		ds.receive(DpReceive);
	
		String msg = new String(DpReceive.getData()).trim();
		
		replyToClient(DpReceive.getAddress(), msg.toUpperCase());
	}
	
	private void replyToClient(InetAddress clientAddress, String msgtoUpperCase) throws IOException {
		DatagramSocket udpSocket = new DatagramSocket();
		byte buf[] = null;
		
		buf = msgtoUpperCase.getBytes();
		DatagramPacket DpSend = new DatagramPacket(buf, buf.length, clientAddress, sendPort);
		
		udpSocket.send(DpSend);
		udpSocket.close();
	}
	
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("-- Running UDP server at " + InetAddress.getLocalHost() + " --");
		int sendPort = 7076;
		int recvPort = 7077;
		UDPServer server = new UDPServer(sendPort, recvPort);
		server.start();
	}

}
