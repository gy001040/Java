package SMTP;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Client {
	private Socket tcpSocket;
	private List<String> initialMessages;
	
	/**
	* @author Viktor
	* @param  tcpSocket  the socket we use to contact the server
	* @param  initialMessages  the messages that we pass to the server initially when we connect
	* @throws IOException, Exception
	*/
	
	//Try making connection to the server
	Client(InetAddress serverAddress, int serverPort) throws Exception {
		tcpSocket = new Socket(serverAddress, serverPort);
		initialMessages = new ArrayList<String>();
		initializeMessages();
	}
	
	//Fill the list with random messages
	private void initializeMessages() {
		initialMessages.add("HELLO reading.ac.uk");
		initialMessages.add("MAIL FROM: <student@reading.ac.uk>");
		initialMessages.add("RCPT TO: <student-loan@gov.uk>");
		initialMessages.add("DATA");
		initialMessages.add("Hello loan team,");
		initialMessages.add("If I don't study, will I lose my loan?");
		initialMessages.add(".");
		initialMessages.add("QUIT");
	}
	
	//Wait for input from the user and send it to the server
	private void start() throws IOException {
		DataOutputStream dout = new DataOutputStream(tcpSocket.getOutputStream());
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		//Send the initial messages
		for(int i = 0; i < initialMessages.size(); i++) {
			String message = initialMessages.get(i);
			dout.writeUTF(message);
		}
		
		//Wait for input
		while(true) {		
			System.out.print("Enter message (type exit to end the connection): ");
			String so = br.readLine();
			dout.writeUTF(so);

			if(so.equals("exit")) break;
		}
		
		tcpSocket.close();
	}
	
	public static void main(String[] args) throws Exception {
		//Initialise the server with the ip of the local host and port 7077 for sending messages
		Client client = new Client(InetAddress.getLocalHost(), 7077);
		client.start();
	}

}
