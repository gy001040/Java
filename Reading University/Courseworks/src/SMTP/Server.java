package SMTP;

import java.io.DataInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Server {
	private ServerSocket server;
	private Socket socket;
	private InetAddress ipServer;
	
	/**
	* @author Viktor
	* @param  server  the server on which the the three-way connection is established
	* @param  socket  the socket we use to receive messages from the user
	* @param  ipServer  the ip on which the connection is established (Local Host)
	* @throws IOException
	*/
	
	//Establish the three-way handshake in the constructor
	Server(InetAddress ip, int port) throws IOException {
		server = new ServerSocket(port);
		socket = server.accept();
		ipServer = ip;
		System.out.println("Server: 220 gov.uk");
	}

	//Listen for requests from the client
	private void listen() throws IOException {
		DataInputStream din = new DataInputStream(socket.getInputStream());
		
		while(true) {
			String yoo = din.readUTF();
			System.out.println("Client: " + yoo);
			
			//Check for the initial messages
			if(yoo.equals("HELLO reading.ac.uk")) {
				System.out.println("Server: 250 Hello reading.ac.uk, pleased to meet you");
			} else if(yoo.equals("MAIL FROM: <student@reading.ac.uk>") || yoo.equals("RCPT TO: <student-loan@gov.uk>")) {
				System.out.println("Server: 250 ok");
			} else if(yoo.equals("DATA")) {
				System.out.println("Server: 354 End data with <CR><LF>.<CR><LF>");
			} else if(yoo.equals(".")) {
				System.out.println("Server: 250 ok Message accepted for delivery");
			} else if(yoo.equals("QUIT")) {
				System.out.println("Server: 221 gov.uk closing connection\n");
			} else if (yoo.equals("exit")) {
				break;
			} else {
				writeToFile(yoo);
			}
			
		}
		
		socket.close();
	}
	
	//Save all the requests from the user to a text file
	private void writeToFile(String message) {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
		
		try {
		      FileWriter myWriter = new FileWriter("gov.uk.txt", true);

		      myWriter.write("Host: " + ipServer.toString() + ", Time: " + dtf.format(LocalDateTime.now()) + ", Message: " +  message + "\n");
		      myWriter.close();
		} catch (IOException e) {
		      e.printStackTrace();
		}
		
	}
	
	public static void main(String[] args) throws IOException {
		//Initialise the server with the ip of the local host and port 7077 for receiving messages
		Server server = new Server(InetAddress.getLocalHost(), 7077);
		server.listen();
	}
	
}
