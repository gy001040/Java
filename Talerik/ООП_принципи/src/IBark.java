
public interface IBark {
    void bark();
    void stopBarking();
    
}
