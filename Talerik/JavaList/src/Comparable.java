
public interface Comparable {

	boolean isLessThan(Object obj);
}
