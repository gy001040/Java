package elements;

public interface Fire {
	
	public Void showStats();
	public Void evolve();

}
