package elements;

public interface Electric {

	public Void showStats();
	public Void evolve();
	
}
