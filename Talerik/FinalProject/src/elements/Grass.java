package elements;

public interface Grass {
	
	public Void showStats();
	public Void evolve();

}
