package elements;

public interface Water {

	public Void showStats();
	public Void evolve();
	
}
