package elements;

public interface Ground {

	public Void showStats();
	public Void evolve();
	
}
